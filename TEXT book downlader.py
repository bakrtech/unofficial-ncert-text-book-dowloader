import wget
Subject_list="""
1)Chemistry
2)Physics
3)Maths
4)Computer Science
5)Economics
6)Biology
"""
print("Welcome to the Unofficial NCERT TEXT BOOK DOWNLOADER")
print("Please select the subject you want to download")
print(Subject_list)
Subject_choice=input("Enter your choice: ")
Chapter_choice=int(input("Enter the chapter number: "))
if Chapter_choice>=1 and Chapter_choice<10:
    Chapter_choice="0"+str(Chapter_choice)
if Subject_choice=="1":
    smlform="ch"
elif Subject_choice=="2":
    smlform="ph"
elif Subject_choice=="3":
    smlform="mh"
elif Subject_choice=="4":
    smlform="cs"
elif Subject_choice=="5":
    smlform="ec"
elif Subject_choice=="6":
    smlform="bo"
link=f"https://ncert.nic.in/textbook/pdf/le{smlform}1{Chapter_choice}.pdf"
print(link)
wget.download(link)
