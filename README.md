# UNOFFICAIL NCERT books downloader

## Installation

```bash

pip install wget
git clone https://gitlab.com/bakrtech/unofficial-ncert-text-book-dowloader
cd unofficial-ncert-text-book-dowloader
python main.py


```

---

## About

This is a python script that will help you to download chapters pdf of various NCERT Subject book

---

## Author

[BakrTech](https://gitlab.com/bakrtech/)
